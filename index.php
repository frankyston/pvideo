<html lang="pt-BR">
<head>
<meta charset="utf-8">

	<title> Sistema de Fotos e Vídeos </title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />

</head>
<body>

<!-- HEADER -->
<div id="header">
	<div class="wrappers">
		HEADER...
	</div>
</div>
<!-- /HEADER -->

<!-- MENU -->
<div id="menu">
	<div class="wrappers">
		<ul>
			<li><a href="fotos">Fotos</a></li>
			<li><a href="videos">Videos</a></li>
		</ul>
	</div>
</div>
<!-- /MENU -->

<!-- CONTENT -->
<div id="content">
	<div class="wrappers">
		CONTEÚDO...
	</div>
</div>
<!-- /CONTENT -->

<!-- FOOTER -->
<div id="footer">
	<div class="wrappers">
		<p>Todos os Direitos Reservados a FRANAT NETWORK - <a href="http://www.franat.com.br">www.franat.com.br</a></p>
	</div>
</div>
<!-- FOOTER -->

</body>
</html>